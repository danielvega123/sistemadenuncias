﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SysDenuncia.Manejo_Base_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace SysDenuncia.Manejo_Base_Datos.Tests
{
    [TestClass()]
    public class SignUpTests
    {
        [TestMethod()]
        public void validarNombreTest()
        {
            SignUp c = new SignUp();
            bool actual = c.validarNombre("hola");
            bool expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void validarCarnetTest()
        {
            SignUp c = new SignUp();
            bool actual = c.validarCarnet("23");
            bool expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void singUpTest()
        {
            SignUp c = new SignUp();
            bool actual = c.singUp("0", "root", "M", "Administrador", "0000");
            bool expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void codigoEncriptadoTest()
        {
            SignUp c = new SignUp();
            string actual = c.codigoEncriptado();
            string expected = "Denuncia0";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void createDenunciaTest()
        {
            SignUp c = new SignUp();
            string actual = c.createDenuncia("","201403785", "201403785","1","1","titulo");
            string notExpected = null;
            Assert.AreNotEqual(notExpected, actual);
        }


        // PRUEBA UNITARIA DE SUBIR ARCHIVOS A LA DENUNCIA

        /*criterios  
             1.      Cuenta Activa
             2.      Denuncia existente a través de código único
             3.      Denuncia de la propiedad del usuario a través de código único
             4.    Tipo de archivo imagen

             */

        // Criterio de Aceptacion 1
        [TestMethod()]
        public void solicitudClientecuentaActivaTest()
        {
            //VALIDAR ID Y CONTRASEÑA
            SignUp prueba = new SignUp();
            prueba.login("201403785", "123456");

            

            //ASSERT
            Assert.IsNotNull(SignUp.cuentaAct);


            //SEGUNDA VALIDACION: LOGOUT, NO SESION ACTIVA

            //ARRANGE
            prueba.logOut();
            
            //ASSERT
            Assert.IsNull(SignUp.cuentaAct);

            //TERCERA VALIDACION: ID CORRECTO, CONTRASEÑA INCORRECTA

            //ARRANGE
            prueba.login("201403785", "111111");


            //ASSERT
            Assert.IsNull(SignUp.cuentaAct);

        }
        
        // Criterio de Aceptacion 2
        [TestMethod()]
        public void CodigoUnicoTest()
        {
            //ARRANGE
            SignUp prueba = new SignUp();
            string codigoUnico = "DDSYS5";
            string codigoUnicoINcorrecto = "SYSD00a45-0078-dfgr";

            //ACT
            bool existeDenuncia = prueba.verificarCodigo(codigoUnico);
            bool existeDenunciaIncorrecta = prueba.verificarCodigo(codigoUnicoINcorrecto);

            //ASSERT
            Assert.AreNotEqual(existeDenuncia, false);
            Assert.AreEqual(existeDenunciaIncorrecta, false);
        }

       
        // Criterio de Aceptacion 3
        [TestMethod()]
        public void CodigoUnicoPropiedadTest()
        {
            //ARRANGE
            SignUp prueba = new SignUp();
            string codigoUnico = "DDSYS5";
            prueba.login("201403785","123456");
            string usuario = SignUp.cuentaAct;
            string usuarioIncorrecto = "20150253";

            //ACT
            bool existeDenuncia = prueba.verificarPropiedad(codigoUnico,usuario);
            bool existeDenunciaIncorrecta = prueba.verificarPropiedad(codigoUnico,usuarioIncorrecto);

            //ASSERT
            Assert.AreNotEqual(existeDenuncia, false);
            Assert.AreEqual(existeDenunciaIncorrecta, false);
        }
        
       // Criterio de Aceptacion 4
       [TestMethod()]
       public void tipoImagenTest()
       {
           //ARRANGE
           SignUp prueba = new SignUp();
           string archivo = "imagen1.jpeg";
           string archivoIncorrecto = "Imagen1.pdf";

           //ACT
           bool correcto = prueba.verificarArchivo(archivo);
           bool incorrecto = prueba.verificarArchivo(archivoIncorrecto);

           //ASSERT
           Assert.AreNotEqual(correcto, false);
           Assert.AreEqual(incorrecto, false);
       }

      

        [TestMethod()]
        public void subirArchTest() {

            //ARRANGE
            SignUp prueba = new SignUp();
            bool valorEsperado;
            bool valorVerdadero;
            prueba.login("201403785", "123456");
            String codigo = "DDSYS5";

            //ACT
            valorEsperado = true;
            FileUpload archi = new FileUpload();
            
            valorVerdadero = prueba.agregarArch(codigo, archi.PostedFile);

            //ASSERT
            Assert.AreEqual(valorEsperado, valorVerdadero);


            //SEGUNDA VALIDACION: Sesion Inactiva

            //ARRANGE
            prueba.logOut();

            //ACT
            valorEsperado = false;
            valorVerdadero = prueba.agregarArch(codigo, archi.PostedFile);

            //ASSERT
            Assert.AreEqual(valorEsperado, valorVerdadero);
        }

       
    }
}