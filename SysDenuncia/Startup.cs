﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SysDenuncia.Startup))]
namespace SysDenuncia
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
