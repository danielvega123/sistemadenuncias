﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using SysDenuncia.Manejo_Base_Datos;
using SysDenuncia.Account;

namespace SysDenuncia
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                MySqlConnection conexion;
                string myConnectionString;

                myConnectionString = "server=104.198.29.58;uid=root;" +
                    "pwd=root; database=prueba";
                conexion = new MySqlConnection(myConnectionString);
                MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT * FROM Categoria", conexion);
                MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
                DataSet ds = new DataSet();
                da.Fill(ds);
                
                this.TITULODENUNCIA.DataSource = ds;
                this.TITULODENUNCIA.DataValueField = "id_categoria";
                this.TITULODENUNCIA.DataTextField = "nombre";
                this.TITULODENUNCIA.DataBind();
                this.TITULODENUNCIA.Items.Insert(0, new ListItem("Elija una Titulo", "0"));

                this.Tipo.Items.Insert(0, new ListItem("Elija una Tipo", "0"));
                this.Tipo.Items.Insert(1, new ListItem("Estudiante", "1"));
                this.Tipo.Items.Insert(2, new ListItem("Trabajador", "2"));
            }
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            SignUp bd = new SignUp();
           // if (!this.SUBIRARCHIVOS.HasFile) {
                String cod = bd.createDenuncia(this.RAZONDENUNCIA.Text, "201403785", this.DENUNCIADO.SelectedValue.ToString(), this.PRIORIDADDENUNCIA.SelectedIndex.ToString(), this.Tipo.SelectedIndex.ToString(), this.TITULODENUNCIA.SelectedValue);
                if (cod != null)
                {
                    this.Page.Response.Write("<script language='JavaScript'>window.alert('Su Codigo es: " + cod + "');</script>");
                }
                else
                {
                    this.Page.Response.Write("<script language='JavaScript'>window.alert('Error');</script>");
                }
          /*  }else
            {
                
                String cod = bd.createDenunciaArch(this.RAZONDENUNCIA.Text, "201403785", this.DENUNCIADO.SelectedValue.ToString(), this.PRIORIDADDENUNCIA.SelectedIndex.ToString(), this.Tipo.SelectedIndex.ToString(), this.TITULODENUNCIA.SelectedValue,this.SUBIRARCHIVOS.PostedFile);
                if (cod != null)
                {
                    this.Page.Response.Write("<script language='JavaScript'>window.alert('Su Codigo es: " + cod + "');</script>");
                }
                else
                {
                    this.Page.Response.Write("<script language='JavaScript'>window.alert('Error');</script>");
                }

            }*/
        }

      

        protected void Tipo_SelectedIndexChanged1(object sender, EventArgs e)
        {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT * FROM Usuario where tipo_usuario=" + this.Tipo.SelectedIndex.ToString() + ";", conexion);
            MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
            DataSet ds = new DataSet();
            da.Fill(ds);

            this.DENUNCIADO.DataSource = ds;
            this.DENUNCIADO.DataValueField = "idUsuario";
            this.DENUNCIADO.DataTextField = "nombre";
            this.DENUNCIADO.DataBind();
            this.DENUNCIADO.Items.Insert(0, new ListItem("Elija una Persona", "0"));
        }
    }


    
}