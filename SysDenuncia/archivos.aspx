﻿<%@ Page Title="SUBIR EVIDENCIA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="archivos.aspx.cs" Inherits="SysDenuncia.archivos" %>

<asp:Content ID="barra" ContentPlaceHolderID="ESPACIOMENU" runat="server">
    <div class="menu_block">
    <nav>
        <ul class="sf-menu">
            <li ><a href="/Default">HOME</a></li>
            <li><a href="/Denuncia">DENUNCIA</a></li>
            <li class="current"><a href="/SUBIR_ARCHIVO">SUBIR EVIDENCIA</a></li>
        </ul>
    </nav>
</div>
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container_12">
    <h2><%: Title %>.</h2>
        <br />
         <div class="grid_7">
    <asp:Label AssociatedControlID="CODIGODENUNCIA" runat="server" Font-Bold="true" Font-Size="Large">CODIGO DENUNCIA</asp:Label>
            <br />
            <asp:TextBox ID="CODIGODENUNCIA" runat="server"  Font-Bold="true" Font-Size="Large" Width="525px" Height="30px" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="CODIGODENUNCIA"
                CssClass="text-danger" ErrorMessage="El titulo es un campo requerido" Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
            <br />
        <br />
        <asp:Label AssociatedControlID="ARCHIVOS" runat="server" Font-Bold="true" Font-Size="Large">EVIDENCIA</asp:Label>
        <br />
            <asp:FileUpload ID="ARCHIVOS" runat="server" Font-Bold="True" Font-Size="Large" Width="364px" />
            <br />
            <asp:Button ID="Button1" runat="server" Text="SUBIR EVIDENCIA"   Width="535px" OnClick="Button1_Click" />
        <br />
             </div>
        </div>
</asp:Content>
