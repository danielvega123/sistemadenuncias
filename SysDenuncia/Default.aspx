﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SysDenuncia._Default" %>


<asp:Content ID="barra" ContentPlaceHolderID="ESPACIOMENU" runat="server">
    <div class="menu_block">
    <nav>
        <ul class="sf-menu">
            <li class="current"><a href="/Default">HOME</a></li>
            <li><a href="/Account/Register">REGISTRO</a> </li>
            <li><a href="/Account/Login">LOGIN</a> </li>
        </ul>
    </nav>
</div>
</asp:Content>

   
<asp:Content ID="Bodycontenido" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container_12">
  <div class="grid_12">
    <div class="slider_wrapper">
      <div>
      <div id="camera_wrap" class="">
               <div data-src="/Content/images/sysSlide.png">
               </div>
                 <div data-src="/Content/images/sysSlide1.png">
                 </div>
               </div>
        </div></div>
      </div>
    </div>

    <div class="page1_block">
  <div class="container_12">
      <div class="grid_3">
      <div class="box ic1 "><div class="maxheight">
        <h3>SYSDENUNCIA</h3><i class=" icon-briefcase"></i>
          <p>Bienvenido a la primera aplicación de denuncias de la universidad de San Carlos. hecha para mejorar la calidad de tus estudios</p>
        <a href="#" class="btn">read more</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic2"><div class="maxheight">
        <h3>¿Quienes Somos?</h3><i class=" icon-umbrella"></i>
          <p>Somos un grupo de estudiantes que velamos por la integridad del proceso académico en la Facultad de Ingeniería.</p>
        <a href="#" class="btn">read more</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic3"><div class="maxheight">
        <h3>Visión</h3><i class=" icon-home"></i>
          <p>Aumentar el nivel académico en base a el cumplimiento de sus deberes tanto de catedráticos como de los estudiantes o auxiliares.</p>
        <a href="#" class="btn">read more</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic4"><div class="maxheight">
        <h3>Misión</h3><i class=" icon-user"></i>
          <p>Establecer un trato de equidad entre estudiantes y catedráticos cumpliendo con las responsabilidades de ambas partes para que la Facultad de Ingeniería tenga tanto catedráticos como futuros profesionales honestos y responsables.</p>
        <a href="#" class="btn">read more</a></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>


</asp:Content>


