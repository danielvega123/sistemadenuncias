﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SysDenuncia.Objeto
{
    public class Sesion
    {


        private static Sesion unicaInstancia;
        public string id;
        public string pwd;
        public int tipo_usuario;

        private Sesion(string id, string pwd, int tipo_usuario)
        {
            this.id = id;
            this.pwd = pwd;
            this.tipo_usuario = tipo_usuario;
        }

        public static Sesion getSesion()
        {
            return unicaInstancia;
        }

        public static void loginSesion(string id, string pwd)
        {
            MySqlConnection conexion;
            DataTable tabla;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT tipo_usuario from Usuario where idUsuario = '" + id + "' and pwd = '" + pwd +"'", conexion);
            MySqlDataAdapter datos = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
            DataSet set = new DataSet();

            datos.Fill(set, "tabla");
            tabla = set.Tables["tabla"];

            if (tabla.Rows.Count != 0)
            {
                unicaInstancia = new Sesion(id, pwd, Int32.Parse(tabla.Rows[0].ItemArray.GetValue(0).ToString()));
            }
        }

        public static void logOut()
        {
            unicaInstancia = null;
        }

    }
}