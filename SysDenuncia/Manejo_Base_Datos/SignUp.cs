﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Data;

namespace SysDenuncia.Manejo_Base_Datos
{
    public class SignUp
    {
        public static string cuentaAct ="201403785";

        public bool singUp(string id, string nombre, string sexo, string tipo, string pwd)
        {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            try
            {
                if (!validarNombre(nombre))
                {
                    return false;
                }
                
                string comando = "INSERT INTO Usuario VALUES (@id, @nombre, @sexo, @tipo, @pwd);";
                MySqlCommand query = new MySqlCommand(comando, conexion);

                query.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
                query.Parameters.Add("@nombre", MySqlDbType.VarChar, 100).Value = nombre;
                query.Parameters.Add("@pwd", MySqlDbType.VarChar, 50).Value = pwd;
                query.Parameters.Add("@sexo", MySqlDbType.VarChar, 1).Value = sexo;
                query.Parameters.Add("@tipo", MySqlDbType.Int32).Value = getTipo(tipo);


                conexion.Open();
                query.ExecuteNonQuery();
                
                //MessageBox.Show("Conexion Exitosa");
                return true;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show("Error, no se pudo conectar");
                return false;
            }

           
        }

        public string createDenuncia(string descripcion, string usuario, string denunciado, string prioridad, string tipo, string titulo)
        {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            try
            {
                if (codigoEncriptado() == null)
                {
                    return null;
                }

                string comando = "INSERT INTO Denuncia(Descripcion,fecha_denuncia,codigo_encriptado,usuario,Denunciado,prioridad,tipo,titulo) VALUES ( @Descripcion, @fecha_denuncia, @codigo_encriptado, @usuario,@denunciado,@prioridad,@tipo,@titulo);";
                MySqlCommand query = new MySqlCommand(comando, conexion);



                query.Parameters.Add("@Descripcion", MySqlDbType.Text).Value = descripcion;
                query.Parameters.Add("@fecha_denuncia", MySqlDbType.DateTime).Value = DateTime.Now;

                string codigo = codigoEncriptado();
                query.Parameters.Add("@codigo_encriptado", MySqlDbType.VarChar, 10).Value = codigo;
                query.Parameters.Add("@usuario", MySqlDbType.Int32).Value = usuario;
                query.Parameters.Add("@Denunciado", MySqlDbType.Int32).Value = denunciado;
                query.Parameters.Add("@prioridad", MySqlDbType.Int32).Value = prioridad;
                query.Parameters.Add("@tipo", MySqlDbType.Int32).Value = tipo;
                query.Parameters.Add("@titulo", MySqlDbType.Int32).Value = titulo;


                conexion.Open();
                query.ExecuteNonQuery();

                return codigo;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show("Error, no se pudo conectar");
                return null;
            }


        }

        public void login(string usuario, string contra) {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT * FROM Usuario where idUsuario=\"" + usuario + "\" and pwd=\"" + contra + "\"", conexion);
            MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
            DataSet ds = new DataSet();
            da.Fill(ds, "tabla");

            DataTable tabla;
            tabla = ds.Tables["tabla"];
            if (tabla.Rows.Count > 0)
            {
                cuentaAct = usuario;
            }
            else {
                cuentaAct = null;
            }
            

        }
        public void logOut()
        {
            cuentaAct = null;
        }

        /*    public string createDenunciaArch(string descripcion, string usuario, string denunciado, string prioridad, string tipo, string titulo, HttpPostedFile archivo)
            {
                MySqlConnection conexion;
                string myConnectionString;

                myConnectionString = "server=104.198.29.58;uid=root;" +
                    "pwd=root; database=prueba";
                conexion = new MySqlConnection(myConnectionString);
                //try
                //{
                    if (codigoEncriptado() == null)
                    {
                        return null;
                    }

                    string comando = "INSERT INTO Denuncia(Descripcion,fecha_denuncia,codigo_encriptado,usuario,Denunciado,prioridad,tipo,titulo, archivos, tipoarch) VALUES ( @Descripcion, @fecha_denuncia, @codigo_encriptado, @usuario,@denunciado,@prioridad,@tipo,@titulo, @arch, @tipoarch);";
                    MySqlCommand query = new MySqlCommand(comando, conexion);

                    string nombre = archivo.FileName.Substring(0, archivo.FileName.LastIndexOf("."));
                    // Extensión del archivo
                    string ext = archivo.ContentType;

                byte[] imagen = new byte[archivo.InputStream.Length];
                    archivo.InputStream.Read(imagen, 0, imagen.Length);

                    query.Parameters.Add("@Descripcion", MySqlDbType.Text).Value = descripcion;
                    query.Parameters.Add("@fecha_denuncia", MySqlDbType.DateTime).Value = DateTime.Now;

                    string codigo = codigoEncriptado();
                    query.Parameters.Add("@codigo_encriptado", MySqlDbType.VarChar, 10).Value = codigo;
                    query.Parameters.Add("@usuario", MySqlDbType.Int32).Value = usuario;
                    query.Parameters.Add("@Denunciado", MySqlDbType.Int32).Value = denunciado;
                    query.Parameters.Add("@prioridad", MySqlDbType.Int32).Value = prioridad;
                    query.Parameters.Add("@tipo", MySqlDbType.Int32).Value = tipo;
                    query.Parameters.Add("@titulo", MySqlDbType.Int32).Value = titulo;
                    query.Parameters.Add("@arch", MySqlDbType.LongBlob).Value = imagen;
                    query.Parameters.Add("@tipoarch", MySqlDbType.VarChar, 25).Value = ext;

                    conexion.Open();
                    query.ExecuteNonQuery();

                    return codigo;
                /*}
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    //MessageBox.Show("Error, no se pudo conectar");
                    return null;
                }


            }*/


        public bool agregarArch(string codigo, HttpPostedFile archivo) {
            if (cuentaAct!=null && verificarCodigo(codigo) && verificarPropiedad(codigo,cuentaAct) && verificarArchivo(archivo.FileName)) {
                MySqlConnection conexion;
                string myConnectionString;
                myConnectionString = "server=104.198.29.58;uid=root;" +
                    "pwd=root; database=prueba";
                conexion = new MySqlConnection(myConnectionString);
                MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT idDenuncia as 'id' FROM Denuncia where codigo_encriptado=\""+codigo+"\"", conexion);
                MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
                DataSet ds = new DataSet();
                da.Fill(ds);
                System.Web.UI.WebControls.DropDownList filtro = new System.Web.UI.WebControls.DropDownList();
                filtro.DataSource = ds;
                filtro.DataValueField = "id";
                filtro.DataTextField = "id";
                filtro.DataBind();
                String newProdID= filtro.SelectedItem.ToString();
               
                try
                {
                string comando = "INSERT INTO Archivo(documento,denuncia,tipo) VALUES (@documento,@denuncia,@tipo);";
                MySqlCommand query = new MySqlCommand(comando, conexion);
                string nombre = archivo.FileName.Substring(0, archivo.FileName.LastIndexOf("."));
                    // Extensión del archivo
                    string[] ext2 = archivo.FileName.Split('.');
                    string ext = ext2[ext2.Length - 1].ToLower();
                byte[] imagen = new byte[archivo.InputStream.Length];
                archivo.InputStream.Read(imagen, 0, imagen.Length);        
                query.Parameters.Add("@documento", MySqlDbType.LongBlob).Value = imagen;
                query.Parameters.Add("@tipo", MySqlDbType.VarChar, 25).Value = ext;
                query.Parameters.Add("@denuncia", MySqlDbType.Int32).Value = newProdID;
                    conexion.Open();
                query.ExecuteNonQuery();
                
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    return false;
                }

                return true;
            }

            return false;
        }
        public string codigoEncriptado()
        {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT max(idDenuncia) as 'MAXIMO' FROM Denuncia", conexion);
            MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
            DataSet ds = new DataSet();
            da.Fill(ds);

            System.Web.UI.WebControls.DropDownList filtro = new System.Web.UI.WebControls.DropDownList();
            filtro.DataSource = ds;
            filtro.DataValueField = "MAXIMO";
            filtro.DataTextField = "MAXIMO";
            filtro.DataBind();
            String newProdID;
            newProdID = "DDSYS" + filtro.SelectedItem;

            //MessageBox.Show("Conexion Exitosa");
            return newProdID;



        }





        public bool verificarPropiedad(string codigo, string usuario) {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT * FROM Denuncia where codigo_encriptado=\""+codigo+"\" and usuario=\""+usuario+"\"", conexion);
            MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
            DataSet ds = new DataSet();
            da.Fill(ds,"tabla");

            DataTable tabla;
            tabla = ds.Tables["tabla"];
            if (tabla.Rows.Count>0) {
                return true;
            }
            return false;

        }
        public bool verificarArchivo(string archivo) {
            // Extensión del archivo
            string[] ext = archivo.Split('.');
            string auxExt = ext[ext.Length-1].ToLower();
            if (auxExt=="png" || auxExt == "jpg" || auxExt == "jpeg" || auxExt == "bmp") {
                return true;
            }

            return false;
        }
        public bool verificarCodigo(string codigo)
        {
            MySqlConnection conexion;
            string myConnectionString;

            myConnectionString = "server=104.198.29.58;uid=root;" +
                "pwd=root; database=prueba";
            conexion = new MySqlConnection(myConnectionString);
            MySqlCommand OrdenSqlSelect = new MySqlCommand("SELECT * FROM Denuncia where codigo_encriptado=\"" + codigo + "\"" , conexion);
            MySqlDataAdapter da = new MySqlDataAdapter(OrdenSqlSelect.CommandText, conexion);
            DataSet ds = new DataSet();
            da.Fill(ds, "tabla");

            DataTable tabla;
            tabla = ds.Tables["tabla"];
            if (tabla.Rows.Count > 0)
            {
                return true;
            }
            return false;

        }


        public bool validarNombre(string nombre)
        {
            nombre = nombre.ToLower();
            return Regex.IsMatch(nombre, @"^([a-zA-Z]|ñ|á|é|í|ó|ú)+$");

        }

        public bool validarCarnet(string id)
        {
            return Regex.IsMatch(id, @"^[0-9]+$");
        }

        public bool validarFecha()
        {
            return true;
        }

        public string getTipo(string tipoUsuario)
        {
            if (tipoUsuario.ToLower().Equals("estudiante")){
                return "3";
            }else if (tipoUsuario.ToLower().Equals("trabajador"))
            {
                return "2";
            }else if (tipoUsuario.ToLower().Equals("administrador"))
            {
                return "1";
            }
            return "4";
        }



    }
}