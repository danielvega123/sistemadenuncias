﻿<%@ Page Title="DENUNCIA" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Denuncia.aspx.cs" Inherits="SysDenuncia.About" %>

<asp:Content ID="barra" ContentPlaceHolderID="ESPACIOMENU" runat="server">
    <div class="menu_block">
    <nav>
        <ul class="sf-menu">
            <li ><a href="/Default">HOME</a></li>
            <li class="current"><a href="/Denuncia">DENUNCIA</a></li>
        </ul>
    </nav>
</div>
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container_12">
        <h2><%: Title %>.</h2>
        <div class="grid_7">
            <asp:Label AssociatedControlID="TITULODENUNCIA" runat="server" Font-Bold="true" Font-Size="Large">TITULO DENUNCIA</asp:Label>
            <br />
            <asp:DropDownList ID="TITULODENUNCIA" runat="server" Height="41px" Width="364px" Font-Bold="True" Font-Size="Medium" >
                        

                        </asp:DropDownList>
            <br />
            <asp:Label AssociatedControlID="RAZONDENUNCIA" runat="server" Font-Bold="true" Font-Size="Large">RAZÓN DENUNCIA</asp:Label>
            <br />
            <asp:TextBox ID="RAZONDENUNCIA" runat="server" Font-Bold="true" Font-Size="Large" Height="171px" Width="525px" TextMode="MultiLine" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="RAZONDENUNCIA"
                CssClass="text-danger" ErrorMessage="La razón de la denuncia es requerida." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
        </div>
        <div class="grid_5">
            <asp:Label AssociatedControlID="PRIORIDADDENUNCIA" runat="server" Font-Bold="true" Font-Size="Large">PRIORIDAD DENUNCIA</asp:Label>
            <br />
             <asp:DropDownList ID="PRIORIDADDENUNCIA" runat="server" Height="41px" Width="364px" Font-Bold="True" Font-Size="Medium" >
                            <asp:ListItem>ALTA</asp:ListItem>
                            <asp:ListItem>MEDIA</asp:ListItem>
                            <asp:ListItem>BAJA</asp:ListItem>

                        </asp:DropDownList>
            <br />
            <br />
            <asp:Label AssociatedControlID="DENUNCIADO" runat="server" Font-Bold="true" Font-Size="Large">PERSONA DENUNCIADA</asp:Label>
            <br />
             <asp:DropDownList ID="DENUNCIADO" runat="server" Height="41px" Width="364px" Font-Bold="True" Font-Size="Medium" >
                        

                        </asp:DropDownList>
            <br />
            <br />

            <asp:Label runat="server" AssociatedControlID="Tipo" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Tipo</asp:Label>
               <br />     

                        <asp:DropDownList ID="Tipo" runat="server" Height="41px" Width="364px" Font-Bold="True" Font-Size="Medium"  AutoPostBack="True" OnSelectedIndexChanged="Tipo_SelectedIndexChanged1">
                          
                        </asp:DropDownList>

                   
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" style="z-index: 1; position: absolute; top: 332px; left: 572px" Text="Denunciar" OnClick="Button1_Click1" />
        </div>
    </div>
</asp:Content>
