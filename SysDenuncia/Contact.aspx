﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="SysDenuncia.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <strong>Facebook:</strong>   <a>https://www.facebook.com/vega.gerard</a><br />
    <address>
        Ciudad Universitaria, zona 12,<br />
         Ciudad de Guatemala<br />
        <abbr title="Phone">telefono:</abbr>
        40711265
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:Support@example.com">gerdanvegro@gmail.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">jlbc_95@hotmail.com</a>
    </address>
</asp:Content>
