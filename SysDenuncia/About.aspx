﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SysDenuncia.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Descripción:</h3>
    <p>
        <meta charset="utf-8" />
        <b id="docs-internal-guid-2393a9c6-df27-b947-d87a-9ee618694001" style="font-weight:normal;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap;">La complejidad de gestionar una denuncia es un trámite demasiado tardado y es de carácter personal y la forma en la que se lleva el proceso es demasiado tardado, si una persona desea saber el estado de su denuncia tendría que dirigirse personalmente para ver el estado de dicha denuncia, para evitar todo estos inconvenientes se creará el software Sistema de Denuncias el cual maneja todas las acciones necesarias para poder gestionar la denuncia de la persona desde la comodidad de su casa teniendo acceso a internet o en cualquier otra parte en la cual tenga acceso a la internet.</span></b></p>
</asp:Content>
