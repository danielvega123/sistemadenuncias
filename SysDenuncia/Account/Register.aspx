﻿<%@ Page Title="REGISTRO" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="SysDenuncia.Account.Register" %>
<asp:Content ID="barra" ContentPlaceHolderID="ESPACIOMENU" runat="server">
    <div class="menu_block">
    <nav>
        <ul class="sf-menu">
            <li ><a href="/Default">HOME</a></li>
            <li class="current"><a href="/Account/Register">REGISTRO</a> </li>
            <li><a href="/Account/Login">LOGIN</a> </li>
        </ul>
    </nav>
</div>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="container_12">
        <div class="grid_10">
        </div>
        <div class="grid_8">
            <h2><%: Title %>.</h2>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </p>

            <div class="form-horizontal">
                <h4>Create a new account</h4>
                <hr />
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Nombre" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Nombre</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Nombre" CssClass="form-control" Height="30px" Width="280px" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Nombre"
                            CssClass="text-danger" ErrorMessage="The name field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Carnet" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Carnet</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Carnet" CssClass="form-control" Height="30px" Width="280px" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Carnet"
                            CssClass="text-danger" ErrorMessage="The carnet field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Tipo" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Tipo</asp:Label>
                    <div class="col-md-10">

                        <asp:DropDownList ID="Tipo" runat="server" Height="34px" Width="280px">
                            <asp:ListItem>Estudiante</asp:ListItem>
                            <asp:ListItem>trabajador</asp:ListItem>
                            <asp:ListItem>Administrador</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Genero" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Género</asp:Label>
                     <div class="col-md-10">

                        <asp:DropDownList ID="Genero" runat="server" Height="34px" Width="280px">
                            <asp:ListItem>M</asp:ListItem>
                            <asp:ListItem>F</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>

                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Password</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" Height="30px" Width="280px" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                            CssClass="text-danger" ErrorMessage="The password field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Confirm password</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" Height="30px" Width="280px" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                        <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" Font-Bold="True" Font-Size="Large" Height="40px" Width="280px" />
                    </div>
                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
