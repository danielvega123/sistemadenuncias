﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SysDenuncia.Manejo_Base_Datos;
namespace SysDenuncia.Account
{
    public partial class Atencion_Cliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void enviar_click(object sender, EventArgs e)
        {
            SignUp programa = new SignUp();

            if (SysDenuncia.Objeto.Sesion.getSesion() == null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No hay una sesion iniciada')", true);
                return;
            }

            if (!programa.isMessageValid(this.Mensaje.InnerText))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Cuerpo de Mensaje Vacio')", true);
                return;
            }

            
            if (programa.sendMessage(this.Mensaje.InnerText, this.Codigo.Text))
            {
                Response.Redirect("/Default");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Codigo Incorrecto')", true);
            }
            
        }
    }
}