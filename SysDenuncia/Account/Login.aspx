﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SysDenuncia.Account.Login" Async="true" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ID="barra" ContentPlaceHolderID="ESPACIOMENU" runat="server">
    <div class="menu_block">
    <nav>
        <ul class="sf-menu">
            <li ><a href="/Default">HOME</a></li>
            <li ><a href="/Account/Register">REGISTRO</a> </li>
            <li class="current"><a href="/Account/Login">LOGIN</a> </li>
        </ul>
    </nav>
</div>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">
     <div class="container_12">
         <h2><%: Title %></h2>
    <div class="row">
        <div class="col-md-8">
            <section id="loginForm">
                <div class="form-horizontal">
                    <h4>Use a local account to log in.</h4>
                    <hr />
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Carnet</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" Height="30px" Width="280px"  />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="The email field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Password</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" Height="30px" Width="280px" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                        </div>
                    </div>
                    <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Carrera" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Carrera</asp:Label>
            <div class="col-md-10">

                <asp:DropDownList ID="Carrera" runat="server" Height="34px" Width="280px">
                    <asp:ListItem>01. Ing. Civil</asp:ListItem>
                    <asp:ListItem>02. Ing Química</asp:ListItem>
                    <asp:ListItem>03. Ing. Mecánica</asp:ListItem>
                    <asp:ListItem>04. Ing Eléctrica</asp:ListItem>
                    <asp:ListItem>05. Ing. Industrial</asp:ListItem>
                    <asp:ListItem>06. Ing. Mec. Eléctrica</asp:ListItem>
                    <asp:ListItem>07. Ing. Mec. Industrial</asp:ListItem>
                    <asp:ListItem>09. Ing. Ciencias y Sis.</asp:ListItem>
                    <asp:ListItem>10. Lic. Mat. Aplicada</asp:ListItem>
                    <asp:ListItem>12. Lic. Fís. Aplicada</asp:ListItem>
                    <asp:ListItem>13. Ing. Electrónica</asp:ListItem>
                    <asp:ListItem>15. Ing. Agroindustrial</asp:ListItem>
                    <asp:ListItem>35. Ing. Ambiental</asp:ListItem>
                </asp:DropDownList>

            </div>
        </div>
<div class="form-group">
            <asp:Label runat="server" AssociatedControlID="rol" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Grupo</asp:Label>
            <div class="col-md-10">

                <asp:DropDownList ID="rol" runat="server" Height="34px" Width="280px">
                    <asp:ListItem>Estudiante</asp:ListItem>
                    <asp:ListItem>Catedratico</asp:ListItem>
                    <asp:ListItem>Trabajador</asp:ListItem>
                    <asp:ListItem>Administrador</asp:ListItem>
                    <asp:ListItem>Auxiliar</asp:ListItem>
                </asp:DropDownList>

            </div>
        </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="checkbox">
                                <asp:CheckBox runat="server" ID="RememberMe" Font-Bold="True" Font-Size="Large" />
                                <asp:Label runat="server" AssociatedControlID="RememberMe" Font-Bold="True" Font-Size="Large">Remember me?</asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-default" Font-Bold="True" Font-Size="Large" Height="40px" Width="280px" />
                        </div>
                    </div>
                    <br />
                </div>
                <p>
                    <%-- Enable this once you have account confirmation enabled for password reset functionality
                    <asp:HyperLink runat="server" ID="ForgotPasswordHyperLink" ViewStateMode="Disabled">Forgot your password?</asp:HyperLink>
                    --%>
                </p>
            </section>
        </div>

        <div class="col-md-4">
            <section id="socialLoginForm">
            </section>
        </div>
    </div>
         </div>
</asp:Content>
