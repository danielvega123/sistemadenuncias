﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using SysDenuncia.Manejo_Base_Datos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SysDenuncia.Models;

namespace SysDenuncia.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            if (this.Password.Text.Equals(this.ConfirmPassword.Text))
            {
                SignUp bd = new SignUp();
                bd.singUp(this.Carnet.Text, this.Nombre.Text, this.Genero.SelectedValue, this.Tipo.SelectedValue, this.Password.Text);
            }
        }
    }
}