﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Atencion_Cliente.aspx.cs" Inherits="SysDenuncia.Account.Atencion_Cliente" %>
<asp:Content ID="barra" ContentPlaceHolderID="ESPACIOMENU" runat="server">
    <div class="menu_block">
    <nav>
        <ul class="sf-menu">
            <li ><a href="/Default">HOME</a></li>
            <li class="current"><a href="/Account/Atencion_Cliente">REGISTRO</a> </li>
            <li><a href="/Account/Login">LOGIN</a> </li>
        </ul>
    </nav>
</div>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="container_12">
        <div class="grid_10">
        </div>
        <div class="grid_8">
            <h2><%: Title %>.</h2>
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </p>

            <div class="form-horizontal">
                <h4>Escriba un mensaje para saber sobre el proceso de su denuncia</h4>
                <hr />
                <asp:ValidationSummary runat="server" CssClass="text-danger" />
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Codigo" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">codigo</asp:Label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="Codigo" CssClass="form-control" Height="30px" Width="280px" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Codigo"
                            CssClass="text-danger" ErrorMessage="The name field is required." Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Mensaje" CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="Large">Mensaje</asp:Label>
                    <div class="col-md-10">
                        <textarea runat="server" ID="Mensaje" CssClass="form-control" Text ="" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Mensaje"
                            CssClass="text-danger" ErrorMessage="El Campo Mensaje es Obligatorio" Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" />
                    </div>
                </div>
                 
                <br />
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="enviar_click" Text="Enviar" CssClass="btn btn-default" Font-Bold="True" Font-Size="Large" Height="40px" Width="280px" />
                    </div>
                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
